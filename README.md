CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The AddEvent module integrates the Add to Calendar button into the Drupal block system. There are plans to add additional Event and Calendar integrations that are provided by the AddEvent service.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

### Add to Calendar Button

1. Navigate to the `Block Layout` screen (Administration > Structure > Block).
2. Click `Place Block` on the region you would like to render an Add to Calendar button. 
3. Configure the `Add to Calendar` block settings based on your use case. If you need more guidance refer to [https://www.addevent.com/documentation](https://www.addevent.com/documentation).
