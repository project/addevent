<?php

declare(strict_types=1);

namespace Drupal\addevent\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define the AddEvent settings.
 */
class AddEventSettings extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'addevent_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::buildForm($form, $form_state);

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#required' => TRUE,
      '#description' => $this->t('Input the AddEvent API token.'),
      '#default_value' => $this->getConfigurationValue('token')
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    parent::submitForm($form, $form_state);

    $this->getConfiguration()->setData(
      $form_state->cleanValues()->getValues()
    )->save();
  }

  /**
   * Get the AddEvent settings configuration value.
   *
   * @param string $key
   *   The configuration key.
   * @param string|array|null $default_value
   *   The default value.
   *
   * @return mixed
   *   The AddEvent settings value.
   */
  protected function getConfigurationValue(
    string $key,
    string|null|array $default_value = NULL
  ): mixed {
    return $this->getConfiguration()->get($key) ?? $default_value;
  }

  /**
   * Get the AddEvent settings configuration.
   *
   * @return \Drupal\Core\Config\Config
   */
  protected function getConfiguration(): Config {
    return $this->config('addevent.settings');
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'addevent.settings'
    ];
  }
}
