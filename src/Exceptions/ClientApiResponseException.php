<?php

declare(strict_types=1);

namespace Drupal\addevent\Exceptions;

/**
 * Define the client API response exception.
 */
class ClientApiResponseException extends \RuntimeException {}
