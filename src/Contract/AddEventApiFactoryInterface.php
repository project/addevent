<?php

declare(strict_types=1);

namespace Drupal\addevent\Contract;

/**
 * Define the AddEvent API factory interface.
 */
interface AddEventApiFactoryInterface {

  /**
   * Create the AddEvent calendar API.
   *
   * @return \Drupal\addevent\Contract\AddEventCalendarApiInterface
   *   The AddEvent calendar API.
   */
  public function createCalendarApi(
    array $configuration = []
  ): AddEventCalendarApiInterface;
}
