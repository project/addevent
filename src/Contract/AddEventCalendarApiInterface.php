<?php

declare(strict_types=1);

namespace Drupal\addevent\Contract;

/**
 * Define the AddEvent calendar API resource.
 */
interface AddEventCalendarApiInterface {

  /**
   * List the AddEvent calendars.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function listCalendars(): array;

  /**
   * Create the AddEvent calendar.
   *
   * @param string $title
   *   The AddEvent calendar title.
   * @param array $query
   *   The AddEvent optional query.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function createCalendar(
    string $title,
    array $query = []
  ): array;

  /**
   * Save the AddEvent calendar.
   *
   * @param string $title
   *   The AddEvent calendar title.
   * @param string $calendar_id
   *   The AddEvent calendar ID.
   * @param array $query
   *   The AddEvent optional query params.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function saveCalendar(
    string $title,
    string $calendar_id,
    array $query = []
  ): array;

  /**
   * Delete the AddEvent calendar.
   *
   * @param string $calendar_id
   *   The AddEvent calendar ID.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \JsonException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deleteCalendar(
    string $calendar_id
  ): array;

  /**
   * List the calendar events.
   *
   * @param string $calendar_id
   *   The AddEvent calendar ID.
   * @param array $query
   *   The AddEvent optional query parameters.
   *
   * @return array
   *   An array of events.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function listCalendarEvents(
    string $calendar_id,
    array $query = []
  ): array;


  /**
   * Create calendar AddEvent event.
   *
   * @param string $title
   *   The AddEvent event title.
   * @param string $calendar_id
   *   The AddEvent calendar ID.
   * @param array $query
   *   The AddEvent optional query.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function createCalendarEvent(
    string $title,
    string $calendar_id,
    array $query = []
  ): array;

  /**
   * Save calendar AddEvent event.
   *
   * @param string $title
   *   The AddEvent event title.
   * @param string $event_id
   *   The AddEvent event ID.
   * @param array $query
   *   The AddEvent optional query.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function saveCalendarEvent(
    string $title,
    string $event_id,
    array $query = []
  ): array;

  /**
   * Delete calendar AddEvent event.
   *
   * @param string $event_id
   *   The AddEvent event ID.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function deleteCalendarEvent(
    string $event_id
  ): array;

  /**
   * List calendar AddEvent subscribers.
   *
   * @param string $calendar_id
   *   The AddEvent calendar ID.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function listCalenderSubscribers(
    string $calendar_id
  ): array;

  /**
   * View calendar AddEvent subscriber.
   *
   * @param string $subscriber_id
   *   The AddEvent user ID.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function viewCalenderSubscriber(
    string $subscriber_id
  ): array;

  /**
   * Delete calendar AddEvent subscriber.
   *
   * @param string $subscriber_id
   *   The AddEvent user ID.
   *
   * @return array
   *   The AddEvent response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function deleteCalenderSubscriber(
    string $subscriber_id
  ): array;
}
