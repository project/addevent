<?php

declare(strict_types=1);

namespace Drupal\addevent\Api;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\addevent\Exceptions\ClientApiResponseException;

/**
 * Define the base API class.
 */
abstract class BaseApi {

  /**
   * Define the API endpoint base URL.
   */
   protected const ENDPOINT_BASE_URL = NULL;

  /**
   * The API base constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param array $configuration
   *   The HTTP client configuration.
   */
  public function __construct(
    protected ClientInterface $client,
    protected array $configuration = []
  ) {}

  /**
   * Format a timestamp to a valid AddEvent date.
   *
   * @param string|int $timestamp
   *   The unix timestamp.
   *
   * @return string
   *   The AddEvent formatted date.
   */
  public static function formatDate(string|int $timestamp): string {
    return DrupalDateTime::createFromTimestamp($timestamp)->format('Y-m-d H:i');
  }

  /**
   * Get the HTTP request configuration.
   *
   * @return string|array|null
   *   The configuration value
   */
  protected function getConfiguration(
    string $name
  ): string|array|null {
    return $this->configuration[$name] ?? NULL;
  }

  /**
   * Make the request to the AddEvent API.
   *
   * @param string|array $path
   *   The path segments.
   * @param string $method
   *   The request method.
   * @param array $body
   *   The request body.
   * @param array $query
   *   The request query.
   *
   * @return string|null
   *   The request response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function makeRequest(
    string|array $path,
    string $method = 'GET',
    array $body = [],
    array $query = []
  ): ?string {
    try {
      $response = $this->client->request(
        $method,
        $this->createUri($path),
        [
          'json' => $body,
          'query' => $query,
          'headers' => [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer {$this->getToken()}",
          ]
        ]
      );

      if (
        $response->getStatusCode() < 200
        || $response->getStatusCode() > 204
      ) {
        throw new \RuntimeException(
          sprintf(
            'Invalid status code %d provided.',
            $response->getStatusCode()
          )
        );
      }
      return $response->getBody()->getContents();
    } catch (\Exception $exception) {
      throw new ClientApiResponseException(
        $exception->getMessage()
      );
    }

    return NULL;
  }

  /**
   * Parse the response content.
   *
   * @param string $response
   *   The HTTP response content.
   *
   * @return array
   *   An array of response data.
   *
   * @throws \JsonException
   */
  protected function parseResponseContent(
    string $response
  ): array {
    return json_decode(
      $response,
      TRUE,
      512,
      JSON_THROW_ON_ERROR
    );
  }

  /**
   * Get the AddEvent token.
   *
   * @return string|null
   *   The AddEvent token, otherwise NULL.
   */
  protected function getToken(): ?string {
    $token = $this->getConfiguration('token');

    if (!isset($token)) {
      throw new \InvalidArgumentException(
        'The AddEvent token is required!'
      );
    }

    return $token ?? NULL;
  }

  /**
   * Create a URI with path segments.
   *
   * @param array|string $path
   *   The path segments.
   *
   * @return string
   *   The fully qualified URI.
   */
  protected function createUri(array|string $path): string {
    if (is_string($path)) {
      $path = [$path];
    }

    return implode(
      DIRECTORY_SEPARATOR,
      array_merge([static::ENDPOINT_BASE_URL], $path)
    );
  }
}
