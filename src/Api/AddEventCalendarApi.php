<?php

declare(strict_types=1);

namespace Drupal\addevent\Api;

use Drupal\addevent\Contract\AddEventCalendarApiInterface;

/**
 * Define the AddEvent Calendar API class.
 */
class AddEventCalendarApi extends BaseApi implements AddEventCalendarApiInterface {

  /**
   * {@inheritDoc}
   */
  protected const ENDPOINT_BASE_URL = 'https://api.addevent.com/calevent/v2';

  /**
   * {@inheritDoc}
   */
  public function listCalendars(): array {
    return $this->parseResponseContent(
      $this->makeRequest(
        path: 'calendars'
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function createCalendar(
    string $title,
    array $body = []
  ): array {
    $body['title'] = $title;

    return $this->parseResponseContent(
      $this->makeRequest(
        path: 'calendars',
        method: 'POST',
        body: $body
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function saveCalendar(
    string $title,
    string $calendar_id,
    array $body = []
  ): array {
    $body['title'] = $title;

    return $this->parseResponseContent(
      $this->makeRequest(
        path: "calendars/$calendar_id",
        method: 'PATCH',
        query: $body
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCalendar(
    string $calendar_id
  ): array {
    return $this->parseResponseContent(
      $this->makeRequest(
        path: "calendars/$calendar_id",
        method: 'DELETE'
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function listCalendarEvents(
    string|array $calendar_id,
    array $query = []
  ): array {
    $query['calendar_ids'] = is_string($calendar_id)
      ? [$calendar_id]
      : $calendar_id;

    return $this->parseResponseContent(
      $this->makeRequest(
        path: 'events',
        query: $query
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function createCalendarEvent(
    string $title,
    string $calendar_id,
    array $body = []
  ): array {
    $body['title'] = $title;
    $body['calendar_id'] = $calendar_id;

    if (!isset($body['datetime_start'])) {
      $body['datetime_start'] = static::formatDate(time());
    }

    return $this->parseResponseContent(
      $this->makeRequest(
        path: 'events',
        method: 'POST',
        body: $body
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function saveCalendarEvent(
    string $title,
    string $event_id,
    array $body = []
  ): array {
    $body['title'] = $title;

    return $this->parseResponseContent(
      $this->makeRequest(
        path: "events/$event_id",
        method: 'PATCH',
        body: $body
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCalendarEvent(
    string $event_id
  ): array {
    return $this->parseResponseContent(
      $this->makeRequest(
        path: "events/$event_id",
        method: 'DELETE',
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function listCalenderSubscribers(
    string $calendar_id
  ): array {
    $query['calendar_ids'] = is_string($calendar_id)
      ? [$calendar_id]
      : $calendar_id;

    return $this->parseResponseContent(
      $this->makeRequest(
        path: 'subscribers',
        query: $query
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function viewCalenderSubscriber(
    string $subscriber_id
  ): array {
    return $this->parseResponseContent(
      $this->makeRequest(
        path: "subscribers/$subscriber_id",
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCalenderSubscriber(
    string $subscriber_id
  ): array {
    return $this->parseResponseContent(
      $this->makeRequest(
        path: "subscribers/$subscriber_id",
        method: 'DELETE'
      )
    );
  }
}
