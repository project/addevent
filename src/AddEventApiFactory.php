<?php

declare(strict_types=1);

namespace Drupal\addevent;

use Drupal\Core\Config\Config;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\addevent\Api\AddEventCalendarApi;
use Drupal\addevent\Contract\AddEventCalendarApiInterface;
use Drupal\addevent\Contract\AddEventApiFactoryInterface;

/**
 * Define the AddEvent API resource factory.
 */
class AddEventApiFactory implements AddEventApiFactoryInterface {

  /**
   * The HTTP client constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration factory service.
   */
  public function __construct(
    protected ClientInterface $client,
    protected ConfigFactory $configFactory
  ) {}

  /**
   * Create the AddEvent calendar API.
   *
   * @return \Drupal\addevent\Contract\AddEventCalendarApiInterface
   *   The AddEvent calendar API.
   */
  public function createCalendarApi(
    array $configuration = []
  ): AddEventCalendarApiInterface {
    $token = $this->getConfiguration()->get('token')
      ?: NULL;

    if (!isset($token)) {
      throw new \RuntimeException(
        'The AddEvent token is required!'
      );
    }

    return new AddEventCalendarApi(
      $this->client,
      ['token' => $token] + $configuration
    );
  }

  /**
   * Get the AddEvent configuration.
   *
   * @return \Drupal\Core\Config\Config
   */
  protected function getConfiguration(): Config {
    return $this->configFactory->get('addevent.settings');
  }
}
