<?php

declare(strict_types=1);

namespace Drupal\addevent\Plugin\Block;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * @Block(
 *   id = "addevent_add_calendar_button",
 *   admin_label = @Translation("Add To Calendar"),
 *   category = "AddEvent",
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class AddToCalendarBlock extends AddEventCalendarBlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return NestedArray::mergeDeep(parent::defaultConfiguration(), [
      'addevent_settings' => [
        'event' => [
          'title' => NULL,
          'end' => NULL,
          'start' => NULL,
          'description' => NULL,
          'timezone' => date_default_timezone_get(),
          'location' => NULL,
          'organizer' => NULL,
          'organizer_email' => NULL,
          'facebook_event' => NULL,
          'all_day_event' => NULL,
          'date_format' => 'MM/DD/YYYY',
          'alarm_reminder' => NULL,
          'recurring' => NULL,
          'calname' => NULL,
          'attendees' => NULL,
          'client' => NULL,
          'status' => 'CONFIRMED',
          'method' => NULL,
          'transp' => 'OPAQUE'
        ]
      ]
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['addevent_settings']['event'] = [
      '#type' => 'details',
      '#weight' => -99,
      '#title' => $this->t('Event Information'),
      '#description' => $this->t('Input a token or string value which will be
        used to create the calendar button.'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['addevent_settings']['event']['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['node'],
      ];
    }
    $event = $this->getEventConfiguration();

    $form['addevent_settings']['event']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#description' => $this->t('Input the title of the event.'),
      '#default_value' => $event['title'],
    ];
    $form['addevent_settings']['event']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Input the description of the event.'),
      '#default_value' => $event['description'],
    ];
    $form['addevent_settings']['event']['start'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start'),
      '#required' => TRUE,
      '#description' => $this->t('Input the start date of the event.'),
      '#default_value' => $event['start'],
    ];
    $form['addevent_settings']['event']['end'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End'),
      '#description' => $this->t('Input the end date of the event. If end is not
        defined, the end date is automatically set to start date plus one hour.'),
      '#default_value' => $event['end'],
    ];
    $form['addevent_settings']['event']['timezone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timezone'),
      '#description' => $this->t('Input the event time zone.'),
      '#default_value' => $event['timezone'],
    ];
    $form['addevent_settings']['event']['location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#description' => $this->t('Input the event location.'),
      '#default_value' => $event['location'],
    ];
    $form['addevent_settings']['event']['organizer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organizer'),
      '#description' => $this->t('Input the event organizer.'),
      '#default_value' => $event['organizer'],
    ];
    $form['addevent_settings']['event']['organizer_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organizer Email'),
      '#description' => $this->t('Input the event organizer e-mail. Required if
        the organizer field has a value!'),
      '#states' => [
        'visible' => [
          ':input[name="settings[addevent_settings][event][organizer]"]' => [
            'filled' => TRUE
          ]
        ]
      ],
      '#default_value' => $event['organizer_email'],
    ];
    $form['addevent_settings']['event']['facebook_event'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Event'),
      '#description' => $this->t('Input the event Facebook link.'),
      '#default_value' => $event['facebook_event'],
    ];
    $form['addevent_settings']['event']['all_day_event'] = [
      '#type' => 'textfield',
      '#title' => $this->t('All Day'),
      '#description' => $this->t('Input true/false if the the event is all day.'),
      '#default_value' => $event['all_day_event'],
    ];
    $form['addevent_settings']['event']['date_format'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Date Format'),
      '#description' => $this->t('Input the format of the date in start and end.
        Accepts the values MM/DD/YYYY or DD/MM/YYYY only.'),
      '#default_value' => $event['date_format'],
    ];
    $form['addevent_settings']['event']['alarm_reminder'] = [
      '#type' => 'number',
      '#title' => $this->t('Alarm Reminder'),
      '#description' => $this->t('Set event reminder. Trigger an event reminder
        e.g. "15" minutes before the event starts.'),
      '#default_value' => $event['alarm_reminder'],
    ];
    $form['addevent_settings']['event']['recurring'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recurring'),
      '#description' => $this->t('Input the event recurring rules. Use
        <a href="@url" target="_blank">TextMagic RRule Generator</a>.', [
          '@url' => 'https://www.textmagic.com/free-tools/rrule-generator'
      ]),
      '#default_value' => $event['recurring'],
    ];
    $form['addevent_settings']['event']['calname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Calendar Name'),
      '#description' => $this->t('Input the custom file naming of the .ics file
        used for e.g. Outlook and Apple Calendar. <br/><strong>Note:</strong> If
        left empty the event title is used.'),
      '#default_value' => $event['calname'],
    ];
    $form['addevent_settings']['event']['attendees'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Attendees'),
      '#description' => $this->t('Input one or multiple attendees (pre-populated)
        to the event. Separate multiple email addresses with a comma.'),
      '#default_value' => $event['attendees'],
    ];
    $form['addevent_settings']['event']['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#description' => $this->t('Define the status parameter in the .ics file
        used for e.g. Outlook and Apple Calendar.'),
      '#options' => [
        'COMPLETED' => $this->t('Completed'),
        'CONFIRMED' => $this->t('Confirmed'),
        'TENTATIVE' => $this->t('Tentative'),
        'CANCELLED' => $this->t('Cancelled'),
        'IN-PROCESS' => $this->t('In Progress'),
        'NEEDS-ACTION' => $this->t('Need Action'),
      ],
      '#default_value' => $event['status'],
    ];
    $form['addevent_settings']['event']['client'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client'),
      '#description' => $this->t('The client field refers to your account ID.'),
      '#default_value' => $event['client'],
    ];
    $form['addevent_settings']['event']['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Method'),
      '#description' => $this->t('If defined, a method parameter is added in the
        .ics file used by e.g. Outlook and Apple Calendar..'),
      '#options' => [
        'PUBLISH' => $this->t('Publish'),
        'REQUEST' => $this->t('Request'),
        'CANCEL' => $this->t('Cancel'),
        'REFRESH' => $this->t('Refresh'),
      ],
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $event['method'],
    ];
    $form['addevent_settings']['event']['transp'] = [
      '#type' => 'select',
      '#title' => $this->t('Transparent'),
      '#description' => $this->t('Determines whether the event appears as "free"
        or "busy" on the users calendar.'),
      '#options' => [
        'OPAQUE' => $this->t('Busy'),
        'TRANSPARENT' => $this->t('Free'),
      ],
      '#default_value' => $event['transp'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $configuration = $this->getAddEventConfiguration();

    return [
      '#theme' => 'calendar_button',
      '#button_type' => 'atc',
      '#button_text' => $configuration['button'],
      '#parameters' => $this->processEventParameters(),
      '#attached' => [
        'library' => ['addevent/add-to-calendar'],
        'drupalSettings' => [
          'addeventatc' => [
            'settings' => $this->buildEventCustomizationSettings()
          ]
        ]
      ],
      '#data_attributes' => array_filter($this->getDataAttributes()),
    ];
  }

  /**
   * Get the AddEvent event configuration.
   *
   * @return array
   *   An array of the AddEvent event configuration.
   */
  protected function getEventConfiguration(): array {
    return $this->getAddEventConfiguration()['event'] ?? [];
  }

  /**
   * Process the event parameters.
   *
   * @return array
   *   An array of event parameters.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function processEventParameters(): array {
    return $this->processTokenValues(array_filter(
      $this->getEventConfiguration()
    ));
  }
}
