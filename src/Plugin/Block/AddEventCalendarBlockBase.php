<?php

declare(strict_types=1);

namespace Drupal\addevent\Plugin\Block;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the AddEvent calendar block base.
 */
abstract class AddEventCalendarBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Token $token,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->token = $token;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return NestedArray::mergeDeep(parent::defaultConfiguration(), [
        'addevent_settings' => [
          'button' => $this->t('Add to Calendar'),
          'customization' => [
            'css' => TRUE,
            'mouse' => FALSE,
            'license' => NULL,
            'providers' => [],
          ],
          'data_attributes' => [
            'data-direct' => NULL,
            'data-dropdown-x' => NULL,
            'data-dropdown-y' => NULL,
            'data-render' => NULL,
            'data-intel' => NULL,
            'data-google-api' => NULL,
            'data-outlook-api' => NULL,
          ]
        ]
      ]);
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['addevent_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('AddEvent Settings'),
      '#tree' => TRUE,
    ];
    $settings = $this->getAddEventConfiguration();

    $form['addevent_settings']['button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#required' => TRUE,
      '#default_value' => $settings['button'],
      '#weight' => -100,
    ];
    $this->attachDataAttributesForm($form, $form_state);
    $this->attachCustomizationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockValidate($form, FormStateInterface $form_state): void {
    parent::blockValidate($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);

    $this->setConfigurationValue(
      'addevent_settings',
      $form_state->getValue('addevent_settings')
    );
  }

  /**
   * Get all the AddEvent configuration.
   *
   * @return array
   *   An array of all the AddEvent configurations.
   */
  protected function getAddEventConfiguration(): array {
    return $this->getConfiguration()['addevent_settings'] ?? [];
  }

  /**
   * Get the AddEvent data attributes.
   *
   * @return array
   *   An array of the AddEvent data attributes.
   */
  protected function getDataAttributes(): array {
    return $this->getAddEventConfiguration()['data_attributes'] ?? [];
  }

  /**
   * Attach the AddEvent data attributes form.
   *
   * @param array $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return \Drupal\addevent\Plugin\Block\AddEventCalendarBlockBase
   */
  protected function attachDataAttributesForm(
    array &$form,
    FormStateInterface $form_state
  ): AddEventCalendarBlockBase {
    $form['addevent_settings']['data_attributes'] = [
      '#type' => 'details',
      '#title' => $this->t('Data Attributes'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $dataAttributes = $this->getDataAttributes();

    $form['addevent_settings']['data_attributes']['data-direct'] = [
      '#type' => 'select',
      '#title' => $this->t('Direct'),
      '#description' => $this->t('The attribute disables the dropdown and makes
        the button a direct link to the service specified.'),
      '#options' => $this->getCalendarProviderOptions(),
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-direct'],
    ];
    $form['addevent_settings']['data_attributes']['data-dropdown-x'] = [
      '#type' => 'select',
      '#title' => $this->t('Dropdown x-axis'),
      '#description' => $this->t(
        'Control the horizontal position of the dropdown.'
      ),
      '#options' => [
        'auto' => $this->t('Auto'),
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-dropdown-x'],
    ];
    $form['addevent_settings']['data_attributes']['data-dropdown-y'] = [
      '#type' => 'select',
      '#title' => $this->t('Dropdown y-axis'),
      '#description' => $this->t(
        'Control the vertical position of the dropdown.'
      ),
      '#options' => [
        'auto' => $this->t('Auto'),
        'up' => $this->t('Up'),
        'down' => $this->t('Down'),
      ],
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-dropdown-y'],
    ];
    $form['addevent_settings']['data_attributes']['data-render'] = [
      '#type' => 'select',
      '#title' => $this->t('Render'),
      '#description' => $this->t(
        'The attribute ensures that the options are generated and displayed at
        load. The attribute furthermore disables the default CSS.'
      ),
      '#options' => [
        'inline-buttons' => $this->t('Inline Button'),
      ],
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-render'],
    ];
    $form['addevent_settings']['data_attributes']['data-intel'] = [
      '#type' => 'select',
      '#title' => $this->t('Intel'),
      '#description' => $this->t(
        'If we can detect which device the user is on (e.g. an iDevice) it makes
        sense to automatically select the "Apple" option on click instead of
        showing the dropdown option.'
      ),
      '#options' => [
        'true' => $this->t('True'),
        'false' => $this->t('False'),
      ],
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-intel'],
    ];
    $form['addevent_settings']['data_attributes']['data-google-api'] = [
      '#type' => 'select',
      '#title' => $this->t('Google API'),
      '#description' => $this->t(
        'The attribute enables the Google Events API for adding events to users
        calendar.'
      ),
      '#options' => [
        'true' => $this->t('True'),
      ],
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-google-api'],
    ];
    $form['addevent_settings']['data_attributes']['data-outlook-api'] = [
      '#type' => 'select',
      '#title' => $this->t('Outlook API'),
      '#description' => $this->t(
        'The attribute enables the Microsoft Events API for adding events to
        users calendar.'
      ),
      '#options' => [
        'true' => $this->t('True'),
      ],
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $dataAttributes['data-outlook-api'],
    ];

    return $this;
  }

  /**
   * Get the AddEvent customization settings.
   *
   * @return array
   *   An array of the AddEvent customization settings.
   */
  protected function getCustomization(): array {
    return $this->getAddEventConfiguration()['customization'] ?? [];
  }

  /**
   * Attach the AddEvent customization form.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return \Drupal\addevent\Plugin\Block\AddEventCalendarBlockBase
   */
  protected function attachCustomizationForm(
    array &$form,
    FormStateInterface $form_state
  ): AddEventCalendarBlockBase {
    $form['addevent_settings']['customization'] = [
      '#type' => 'details',
      '#title' => $this->t('Customization'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $customization = $this->getCustomization();

    $form['addevent_settings']['customization']['mouse'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mouse'),
      '#description' => $this->t('Show options dropdown on mouseover.'),
      '#default_value' => $customization['mouse'],
    ];
    $form['addevent_settings']['customization']['css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('CSS'),
      '#description' => $this->t('Use the default add to calendar styles.'),
      '#default_value' => $customization['css'],
    ];
    $form['addevent_settings']['customization']['license'] = [
      '#type' => 'textfield',
      '#title' => $this->t('License'),
      '#description' => $this->t('Input the client account ID. Removes AddEvent
        branding.'),
      '#default_value' => $customization['license'],
    ];
    $form['addevent_settings']['customization']['providers'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Provider'),
        $this->t('Show'),
        $this->t('Text'),
        $this->t('Weight'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];
    $calendarProviders = array_replace_recursive(
      $this->getCalendarProviders(),
      $customization['providers'] ?? []
    );
    $providers = &$form['addevent_settings']['customization']['providers'];

    foreach ($this->sortCalendarProviders($calendarProviders) as $name => $provider) {
      if (!isset($provider['label'])) {
        continue;
      }
      $providerLabel = $provider['label'];
      $providerWeight = $provider['weight'] ?? 0;

      $providers[$name]['#weight'] = $providerWeight;
      $providers[$name]['#attributes']['class'][] = 'draggable';

      $providers[$name]['provider'] = [
        '#plain_text' => $providerLabel,
      ];
      $providers[$name]['show'] = [
        '#type' => 'checkbox',
        '#default_value' => $provider['show'],
      ];
      $providers[$name]['text'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => $provider['text'] ?? $providerLabel
      ];
      $providers[$name]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @label', [
          '@label' => $providerLabel,
        ]),
        '#title_display' => 'invisible',
        '#default_value' => $providerWeight,
        '#attributes' => [
          'class' => [
            'table-sort-weight',
          ],
        ],
      ];
    }

    return $this;
  }

  /**
   * Build the AddEvent customization settings.
   *
   * @return array
   *   An array of formatted customization settings.
   */
  protected function buildEventCustomizationSettings(): array {
    $order = [];
    $settings = [];
    $customization = $this->getCustomization();

    foreach ($customization['providers'] as $name => $provider) {
      if (!isset($provider['show']) || !isset($provider['text'])) {
        continue;
      }

      if ($show = $provider['show']) {
        $order[$name] = $provider['weight'] ?? 0;
        $settings[$name] = [
          'show' => (bool) $show,
          'text' => (string) $provider['text']
        ];
      }
    }
    asort($order);

    return [
      'css' => (bool) $customization['css'],
      'mouse' => (bool) $customization['mouse'],
      'license' => (string) $customization['license'],
      'dropdown' => [
        'order' => (string) implode(',', array_keys($order))
      ]
    ] + $settings;
  }

  /**
   * Get the AddEvent calendar providers.
   *
   * @return array
   *   An array of the AddEvent calendar providers.
   */
  protected function getCalendarProviders(): array {
    return [
      'appleical' => [
        'label' => $this->t('Apple'),
        'show' => TRUE,
        'weight' => 0
      ],
      'google' => [
        'show' => TRUE,
        'label' => $this->t('Google'),
        'text' => $this->t('Google <em>(online)</em>'),
        'weight' => 0
      ],
      'office365' => [
        'show' => 1,
        'label' => $this->t('Office 365'),
        'text' => $this->t('Office 365 <em>(online)</em>'),
        'weight' => 0
      ],
      'outlook' => [
        'show' => TRUE,
        'label' => $this->t('Outlook'),
        'weight' => 0
      ],
      'outlookcom' => [
        'show' => TRUE,
        'label' => $this->t('Outlook.com'),
        'text' => $this->t('Outlook.com <em>(online)</em>'),
        'weight' => 0
      ],
      'yahoo' => [
        'show' => FALSE,
        'label' => $this->t('Yahoo'),
        'text' => $this->t('Yahoo <em>(online)</em>'),
        'weight' => 0
      ],
      'facebook' => [
        'show' => FALSE,
        'label' => $this->t('Facebook'),
        'weight' => 0
      ],
    ];
  }

  /**
   * Get the AddEvent calendar provider options.
   *
   * @return array
   *   An array of the AddEvent provider options.
   */
  protected function getCalendarProviderOptions(): array {
    $options = [];

    foreach ($this->getCalendarProviders() as $name => $provider) {
      if (!isset($provider['label'])) {
        continue;
      }
      $options[$name] = $provider['label'];
    }

    return $options;
  }

  /**
   * Sort the AddEvent calendar providers.
   *
   * @param array $providers
   *   An array of AddEvent calendar providers.
   *
   * @return array
   *   An array of calendar providers sorted by the weight value.
   */
  protected function sortCalendarProviders(array $providers): array {
    uasort($providers, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return $a['weight'] > $b['weight'] ? 1 : -1;
    });

    return $providers;
  }

  /**
   * Process an array of token values.
   *
   * @param array $values
   *   An array of string values.
   *
   * @return array
   *   An array of values with the tokens replaced.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function processTokenValues(array $values): array {
    $data = [];

    if ($node = $this->getBlockNodeContext()) {
      $data['node'] = $node;
    }

    foreach ($values as &$value) {
      $value = $this->token->replace(
        $value, $data, ['clear' => TRUE]
      );
    }

    return $values;
  }

  /**
   * Get the node entity from the context.
   *
   * @return EntityInterface|bool
   *   The Drupal entity instance; otherwise FALSE if context is empty.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  protected function getBlockNodeContext() {
    $context = $this->getContext('node');

    if (!$context->hasContextValue()) {
      return FALSE;
    }

    return $context->getContextData()->getValue();
  }
}
