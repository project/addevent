<?php

declare(strict_types=1);

namespace Drupal\addevent\Plugin\Block;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * @Block(
 *   id = "addevent_subscribe_calendar_button",
 *   admin_label = @Translation("Subscribe To Calendar"),
 *   category = "AddEvent",
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class SubscribeToCalendarBlock extends AddEventCalendarBlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return NestedArray::mergeDeep(parent::defaultConfiguration(), [
      'addevent_settings' => [
        'data_attributes' => [
          'data-id' => NULL,
        ]
      ]
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $dataAttributes = $this->getDataAttributes();

    $form['addevent_settings']['data_attributes']['#open'] = TRUE;

    if ($this->moduleHandler->moduleExists('token')) {
      $form['addevent_settings']['data_attributes']['tokens'] = [
        '#weight' => -100,
        '#theme' => 'token_tree_link',
        '#token_types' => ['node'],
      ];
    }

    $form['addevent_settings']['data_attributes']['data-id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data-ID'),
      '#required' => TRUE,
      '#description' => $this->t('Input the subscription calendar unique ID.'),
      '#default_value' => $dataAttributes['data-id'],
      '#weight' => -100,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $configuration = $this->getAddEventConfiguration();

    return [
      '#theme' => 'calendar_button',
      '#button_type' => 'stc',
      '#button_text' => $configuration['button'],
      '#attached' => [
        'library' => ['addevent/subscribe-to-calendar'],
        'drupalSettings' => [
          'addeventstc' => [
            'settings' => $this->buildEventCustomizationSettings()
          ]
        ]
      ],
      '#data_attributes' => $this->processDataAttributes(),
    ];
  }

  /**
   * Process the data attribute values.
   *
   * @return array
   *   An array of the processed data attributes.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function processDataAttributes(): array {
    return $this->processTokenValues(array_filter(
      $this->getDataAttributes()
    ));
  }
}
